#!/bin/bash 
#
#SBATCH -J gene_family
#SBATCH -o gene_family."%j".out
#SBATCH -e gene_family."%j".err 

# Partition name
#SBATCH -p agap_long

module purge

module load snakemake/7.15.1-conda
 
mkdir -p logs/ 
 
# Print shell commands
#snakemake --profile profile  --jobs 200 --printshellcmds --dryrun --use-envmodules --cores 1

# Unlock repository if one job failed
# snakemake   --profile profile --jobs 200 --unlock  --use-envmodules

# Create DAG file
#snakemake  --profile profile --jobs 2 --dag  --use-envmodules | dot -Tpng > dag.png

# Run workflow
snakemake --profile profile --jobs 60 --cores 140 -p --use-envmodules