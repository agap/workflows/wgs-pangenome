# Snakemake workflow: Pangenomics constructions

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](http://www.gnu.org/licenses/gpl.html)       

**Table of Contents**
 
  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Overview of programmed workflow](#overview-of-programmed-workflow)
  - [Procedure](#procedure)

## Objective

Perform genome assembly on WGS data by accession & pangenome construction

## Dependencies

* fastp : A tool designed to provide fast all-in-one preprocessing for FastQ files. This tool is developed in C++ with multithreading supported to afford high performance (https://github.com/OpenGene/fastp)
* samtools : Reading/writing/editing/indexing/viewing SAM/BAM/CRAM format (http://www.htslib.org/)
* bowtie2 : An ultrafast and memory-efficient tool for aligning sequencing reads to long reference sequences. (http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* masurca : The MaSuRCA (Maryland Super Read Cabog Assembler) genome assembly and analysis toolkit contains of MaSuRCA genome assembler, QuORUM error corrector for Illumina data, POLCA genome polishing software, Chromosome scaffolder, jellyfish mer counter, and MUMmer aligner. (https://github.com/alekseyzimin/masurca)
* megahit : An ultra-fast and memory-efficient NGS assembler.  (https://github.com/voutcn/megahit/)
* cdhit : CD-HIT is a widely used program for clustering biological sequences to reduce sequence redundancy and improve the performance of other sequence analyses. (http://cd-hit.org)
* NCBI-Blast : Basic Local Alignment Search Tool (https://blast.ncbi.nlm.nih.gov/Blast.cgi)

## Overview of programmed workflow

<p align="center">
<img src="images/dag.png" width="50%">
</p>

## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
  ```
  git clone https://gitlab.cirad.fr/umr_agap/workflows/wgs-pangenome.git
  ```


- Change directories into the cloned repository:
  ```
  cd wgs-pangenome
  ```

- Edit the configuration file config.yaml

```
eference: "/storage/replicated/cirad/web/HUBs/yam/Dioscorea_alata/Dalata_550_v2.0.assembly.fna"
chloroplast: "/storage/replicated/cirad/projects/RT/reference_snakemake/chloroplast.fna"
nt: "/lustre/agap/BANK/nt/nt"
memory: "16G"
threads: 8
length: 1000
centrifuge_db: "/home/drocg/scratch/wgs/nt"
taxids: 35493
coverage: 90
assembler: "masurca"
fasta_split_number: 20
taxonomy: "/lustre/agap/BANK/nt/ncbi_taxonomy/expanded_ncbi_taxonomy.tsv"

modules:
  bowtie2: "bowtie2/2.4.2"
  fastp: "fastp/0.20.1"
  trimmomatic: "trimmomatic/0.39"
  samtools: "samtools/1.14-bin"
  megahit: "megahit/1.2.9-bin"
  masurca: "masurca/4.0.1 perllib/5.16.3"
  bbmap: "bbmap/38.90"
  centrifuge: "centrifuge/1.0.4"
  blast: "ncbi-blast/2.12.0+"
  meme: "meme/5.3.3"
  quast: "quast/5.0.2"
  cdhit: "cd-hit/4.8.1"
  multiqc: "multiqc/1.9"
  basta: "basta/1.3.2.3"
  perl: "perllib/5.16.3"
  seqkit: "seqkit/2.0.0"

```

- Print shell commands to validate your modification

```
module load snakemake/7.15.1-conda
snakemake --profile profile  --jobs 200 --printshellcmds --dryrun --use-envmodules --cores 1
```

- Run workflow on Meso@LR

```
sbatch snakemake.sh
```
 
