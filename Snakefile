import pandas as pd
import os,sys
 
configfile: "config.yaml"

path = workflow.basedir
assembler = config["assembler"]
samples = pd.read_csv("metadata.tsv",sep='\t').set_index(["prefix"], drop=False)

wildcard_constraints:
    prefix = "|".join(samples.index),
    sample = "|".join(samples["sample"]),
    assembler=config["assembler"]

def get_fastq(wildcards):
    fastqs = samples.loc[(wildcards.prefix), ["fq1", "fq2"]].dropna()
    return {"read1": fastqs.fq1, "read2": fastqs.fq2}

rule all:
    input:
        expand("{prefix}/fasta/{prefix}_{assembler}_raw.fasta",prefix=samples.index, assembler=config["assembler"]),
        expand("{prefix}/blast/{prefix}_{assembler}_contig.out",prefix=samples.index, assembler=config["assembler"]),
        expand("{prefix}/taxonomy_{assembler}/taxonomy_assignment_per_sequence.tsv", prefix=samples.index, assembler=config["assembler"]),
        expand("{prefix}/fasta/{prefix}_{assembler}.fna" ,prefix=samples.index, assembler=config["assembler"]),
        expand("{prefix}/quast_{assembler}/report.html",prefix=samples.index, assembler=config["assembler"]),
        expand("{prefix}/multiqc_{assembler}/multiqc_report.html",prefix=samples.index, assembler=config["assembler"]),
        expand("result/pangenome_{assembler}_complete.fna", assembler=config["assembler"])
        

rule bowtie_build:
    input:
        reference=config["reference"]
    output:
        bt2=config["reference"]+".1.bt2"
    envmodules:
        config["modules"]["bowtie2"]
    shell:""" 
        bowtie2-build {input.reference} {input.reference}
    """

rule fastp:
    input:
        unpack(get_fastq)    
    output:
        r1 = "{prefix}/fastp/{prefix}_R1.fastq",
        r2 = "{prefix}/fastp/{prefix}_R2.fastq",
        unpaired1  = "{prefix}/fastp/{prefix}_unpaired_R1.fastq",
        unpaired2 = "{prefix}/fastp/{prefix}_unpaired_R2.fastq",    
        json =  "{prefix}/fastp/{prefix}.json",
        html = temp("{prefix}/fastp/{prefix}.html")
    threads: 
        config["threads"] 
    envmodules:
        config["modules"]["fastp"]
    shell:""" 
        fastp --thread {threads} --in1 {input.read1} --in2 {input.read2} --out1 {output.r1} --out2 {output.r2} --unpaired1 {output.unpaired1} --unpaired2 {output.unpaired2} --json {output.json} --html {output.html}
    """
    
    
rule bowtie2:
    input:
        r1 = rules.fastp.output.r1,
        r2 = rules.fastp.output.r2,
        reference=config["reference"]
    output:
        sam = "{prefix}/bowtie2/{prefix}.sam",
        log = "{prefix}/bowtie2/{prefix}.log"
    threads: 
        config["threads"] 
    envmodules:
        config["modules"]["bowtie2"]
    shell:""" 
        bowtie2 -I 0 -X 1000 -x {input.reference} -1 {input.r1} -2 {input.r2} --threads {threads} --end-to-end --sensitive -S {output.sam} 2> {output.log}
    """
    
rule samtools_flagstat:
    input:
        sam = rules.bowtie2.output.sam
    output:
        flagstat = "{prefix}/bowtie2/{prefix}.flagstat"
    threads: 
        config["threads"] 
    envmodules:
        config["modules"]["samtools"]
    shell:""" 
        samtools flagstat --threads {threads} {input.sam} > {output.flagstat} 
    """
    
        
rule samtools_fastq:
    input:
        sam = rules.bowtie2.output.sam
    output:
        R1 = "{prefix}/samtools/{prefix}_R1.fastq",
        R2 = "{prefix}/samtools/{prefix}_R2.fastq",
        R1_single = "{prefix}/samtools/{prefix}_R1_single.fastq",
        R2_single = "{prefix}/samtools/{prefix}_R2_single.fastq",
        map2reference = "{prefix}/samtools/{prefix}.bam"
    threads: 
        config["threads"]   
    envmodules:
        config["modules"]["samtools"]      
    shell:""" 
        samtools fastq --threads {threads} -f 12 {input.sam} -1 {output.R1} -2 {output.R2} 
        samtools fastq --threads {threads} -f 68 -F 8 {input.sam} > {output.R1_single}
        samtools fastq --threads {threads} -f 132 -F 8 {input.sam} > {output.R2_single}
        samtools view --threads {threads} -f 8 -F 4 {input.sam} > {output.map2reference}
    """

rule megahit:
    input:
        R1 = rules.samtools_fastq.output.R1,
        R2 = rules.samtools_fastq.output.R2,
        R1_mate = rules.samtools_fastq.output.R1_single,
        R2_mate = rules.samtools_fastq.output.R2_single
    output:
        fasta = "{prefix}/megahit/final.contigs.fa"
    params:
        outdir = str("{prefix}/megahit"),
        length = config["length"]
    threads: 
        config["threads"] 
    envmodules:
        config["modules"]["megahit"]
    shell:""" 
        rm -Rf {params.outdir};
        megahit  --min-contig-len {params.length} -m 0.9 -t {threads} -1 {input.R1} -2 {input.R2} -r {input.R1_mate},{input.R2_mate} --out-dir {params.outdir}
    """


rule masurca_config:
    input:
        R1 = rules.samtools_fastq.output.R1,
        R2 = rules.samtools_fastq.output.R2,
        R1_mate = rules.samtools_fastq.output.R1_single,
        R2_mate = rules.samtools_fastq.output.R2_single
    output:
        cfg = path+"/{prefix}/masurca/masurca_config.txt"
    run:
        fh_out = open(output[0], "w") 
        fh_out.write('DATA\n')
        fh_out.write('PE= pe 300 50 '+ path +"/"+ input[0]+ ' ' + path +"/"+ input[1]  +'\n')
        fh_out.write('PE= s1 300 50 '+ path +"/"+  input[2] + '\n')
        fh_out.write('PE= s2 300 50 '+ path +"/"+ input[3] + '\n')
        
        fh_out.write('END\n')
        fh_out.write('PARAMETERS\n')
        fh_out.write('GRAPH_KMER_SIZE=auto\n')
        fh_out.write('USE_LINKING_MATES=1\n')
        fh_out.write('KMER_COUNT_THRESHOLD = 1\n')
        fh_out.write('NUM_THREADS=24\n')
        fh_out.write('JF_SIZE=200000000\n')
        fh_out.write('DO_HOMOPOLYMER_TRIM=0\n') 
        fh_out.write('END\n')
        
rule masurca:
    input:
        cfg = rules.masurca_config.output.cfg
    output:
        sh = path+"/{prefix}/masurca/assemble.sh"
    params:
        directory = path+"/{prefix}/masurca"
    envmodules:
        config["modules"]["masurca"]
    shell:""" 
        cd {params.directory};
        masurca masurca_config.txt
    """  
rule masurca_run:
    input:
        cfg = rules.masurca.output.sh
    output:
        fasta = "{prefix}/masurca/CA/final.genome.scf.fasta"
    params:
        directory =  path+"/{prefix}/masurca"
    threads:24
    envmodules:
        config["modules"]["masurca"]
    shell:""" 
        cd {params.directory}; 
        ./assemble.sh
    """
rule filtered_contig:
    input:
        #fasta = rules.masurca_run.output.fasta
        rules.masurca_run.output.fasta if config["assembler"] == 'masurca' else rules.megahit.output.fasta
    output:
        filtered_fasta = "{prefix}/fasta/{prefix}_{assembler}_raw.fasta"
    params:
        assembler = config["assembler"],
        length = config["length"]
    envmodules:
        config["modules"]["bbmap"]
    shell:""" 
        reformat.sh in={input} out={output.filtered_fasta} minlength={params.length}
    """


rule fasta_make_index:
    input:
        fasta = rules.filtered_contig.output.filtered_fasta 
    output:
        index = "{prefix}/fasta/{prefix}_{assembler}_raw.fasta.index",
        fai = "{prefix}/fasta/{prefix}_{assembler}_raw.fasta.fai"     
    params:
        assembler = config["assembler"]   
    envmodules:
        config["modules"]["meme"],
        config["modules"]["samtools"],
        config["modules"]["blast"]
    shell:""" 
        fasta-make-index {input.fasta}
        samtools faidx {input.fasta}
        makeblastdb -in {input.fasta} -dbtype nucl
    """

rule remove_redundancy:
    input:
        fasta = rules.filtered_contig.output.filtered_fasta,
        index = rules.fasta_make_index.output.index
    output:
        blast ="{prefix}/blast/{prefix}_{assembler}_contig.out"  
    threads:
        config["threads"]
    envmodules:
        config["modules"]["blast"]
    shell:""" 
        blastn -query {input.fasta} -db {input.fasta} -num_threads {threads} -dust yes -evalue 1e-5 -max_target_seqs 5 -outfmt '6 qseqid sseqid qlen slen length qstart qend sstart send pident evalue' -out {output.blast}
    """
    
rule filter_blast_redundant_contig:
    input:
        blast = rules.remove_redundancy.output.blast,
        index = rules.fasta_make_index.output.index
    output: 
        list_to_keep = temp("{prefix}/blast/{prefix}_{assembler}_to_keep_filter_contig.txt")        
    params:
        coverage = config["coverage"]
    envmodules:
        config["modules"]["samtools"],
        config["modules"]["perl"]
        
    shell:""" 
        perl script/filter_blast.pl {input.blast} {params.coverage} {input.index} {output.list_to_keep}
    """


rule fasta_fetch_redundant_contig:
    input:
        fasta = rules.filtered_contig.output.filtered_fasta,
        list  = rules.filter_blast_redundant_contig.output.list_to_keep
    output:
        fasta = "{prefix}/fasta/{prefix}_{assembler}_filter_contig.fna",  
        index = "{prefix}/fasta/{prefix}_{assembler}_filter_contig.fna.index"
    envmodules:
        config["modules"]["meme"]        
    shell:""" 
        fasta-fetch {input.fasta} -f {input.list} > {output.fasta}
        fasta-make-index {output.fasta}
    """

rule remove_reference:
    input:
        fasta = rules.fasta_fetch_redundant_contig.output.fasta,
        reference=config["reference"],
        chloroplast=config["chloroplast"]  
    output:
        blast ="{prefix}/blast/{prefix}_{assembler}_reference.out"    
    threads:
        config["threads"]
    envmodules:
        config["modules"]["blast"]
    shell:""" 
        blastn -query {input.fasta} -db '{input.chloroplast} {input.reference}' -num_threads {threads} -dust yes -evalue 1e-10 -max_target_seqs 5 -outfmt '6 qseqid sseqid qlen slen length qstart qend sstart send pident evalue' -out {output.blast}
    """
    
rule filter_blast_reference:
    input:
        blast = rules.remove_reference.output.blast,
        index = rules.fasta_fetch_redundant_contig.output.index
    output: 
        list_to_keep = temp("{prefix}/blast/{prefix}_{assembler}_to_keep_reference.txt")        
    params:
        coverage = config["coverage"]
    envmodules:
        config["modules"]["samtools"],
        config["modules"]["perl"]
        
    shell:""" 
        perl script/filter_blast.pl {input.blast} {params.coverage} {input.index} {output.list_to_keep}
        
    """

rule fasta_fetch_remove_reference:
    input:
        fasta = rules.filtered_contig.output.filtered_fasta,
        list  = rules.filter_blast_reference.output.list_to_keep
    output:
        fasta = "{prefix}/fasta/{prefix}_{assembler}_filtered.fna",   
        index = "{prefix}/fasta/{prefix}_{assembler}_filtered.fna.index"   
    envmodules:
        config["modules"]["meme"]        
    shell:""" 
        fasta-fetch {input.fasta} -f {input.list} > {output.fasta}
        fasta-make-index {output.fasta}
        
    """

rule samtools_faidx:
    input:
        fasta = rules.fasta_fetch_remove_reference.output.fasta 
    output: 
        fai = "{prefix}/fasta/{prefix}_{assembler}_filtered.fna.fai"
    envmodules: 
        config["modules"]["samtools"]
    shell:"""   
        samtools faidx {input.fasta}
    """

 

#fasta = rules.filtered_contig.output.filtered_fasta
rule blastn_nt:
    input:
        fasta = "{prefix}/fasta/{prefix}_{assembler}_filtered.fna" 
    output:
        blast = "{prefix}/blast/{prefix}_{assembler}_nt.out"  
    params:
        nt=config["nt"]
    threads:
        config["threads"]
    envmodules:
        config["modules"]["blast"]
    shell:""" 
        blastn -query {input.fasta} -db {params.nt} -num_threads {threads} -dust yes -evalue 1e-10  -max_target_seqs 1 -outfmt "6 qseqid qlen sseqid pident length qstart qend sstart send evalue bitscore slen staxids" -out {output.blast}
    """


rule assign_taxonomy:
    input:
        blast = rules.blastn_nt.output.blast,
        fasta = rules.fasta_fetch_remove_reference.output.fasta,
        taxonomy = config["taxonomy"]
    output:
        tsv = "{prefix}/taxonomy_{assembler}/taxonomy_assignment_per_sequence.tsv",
        lca = "{prefix}/taxonomy_{assembler}/taxonomy_assignment_per_sequence.lca",
        list_to_keep = "{prefix}/blast/{prefix}_{assembler}_to_keep.txt" 
    params:
        outdir = "{prefix}/taxonomy_{assembler}"
    shell:"""
        rm -Rf {params.outdir}
        python3 script/taxonomy_assignment_BLAST.py --ncbi_nt --output_dir {params.outdir}  --blast_file {input.blast} {input.fasta} {input.taxonomy}
        grep 'Streptophyta' {output.tsv}  | cut -f 1 -d ' ' > {output.list_to_keep}
        cut -f 1,2 -d ' ' {output.tsv}  | sed -e 's/ /\\t/g' >{output.lca}
    """
rule basta2krona:
    input:
        lca = rules.assign_taxonomy.output.lca
    output:
        html = "{prefix}/taxonomy_{assembler}/{prefix}_{assembler}.html"
    params:
        assembler = config["assembler"]
    envmodules:
        config["modules"]["basta"]
    shell:"""
        basta2krona.py  {input.lca} {output.html}
    """


rule fasta_fetch_final:
    input:
        fasta = rules.filtered_contig.output.filtered_fasta,
        list  = rules.assign_taxonomy.output.list_to_keep
    output:
        fasta = "{prefix}/fasta/{prefix}_{assembler}.fna",
        index = "{prefix}/fasta/{prefix}_{assembler}.fna.index"
    envmodules:
        config["modules"]["meme"]        
    shell:""" 
        fasta-fetch {input.fasta} -f {input.list} > {output.fasta}
        fasta-make-index {output.fasta}
        
    """


rule quast:
    input:
        raw_assembly = rules.filtered_contig.output.filtered_fasta,
        remove_redundancy = rules.fasta_fetch_redundant_contig.output.fasta,
        remove_reference = rules.fasta_fetch_remove_reference.output.fasta,
        final = rules.fasta_fetch_final.output.fasta
    output:
        txt = "{prefix}/quast_{assembler}/report.txt" ,
        tsv = "{prefix}/quast_{assembler}/report.tsv",
        html = "{prefix}/quast_{assembler}/report.html"
    threads: 
        config["threads"]
    params:
        directory = "{prefix}/quast_{assembler}",
        labels = " --labels ' Raw assembly, Remove contig redundancy, Remove contig similar to reference, Remove contig out Streptophyta '"
    envmodules:
        config["modules"]["quast"]        
    shell:"""  
        quast.py -o {params.directory} --threads {threads} {params.labels} {input.raw_assembly} {input.remove_redundancy} {input.remove_reference} {input.final} 
    """

rule multiqc:
    input:
        #expand("{prefix}/fastp/{prefix}.json", prefix=samples.index,assembler=config["assembler"]),
        #expand("{prefix}/bowtie2/{prefix}.log",prefix=samples.index,assembler=config["assembler"]),
        #expand("{prefix}/bowtie2/{prefix}.flagstat", prefix=samples.index,assembler=config["assembler"]),
        "{prefix}/quast_{assembler}/report.tsv"
    output:
        protected("{prefix}/multiqc_{assembler}/multiqc_report.html")
    envmodules:
        config["modules"]["multiqc"]
    params:
        directory = "{prefix}/multiqc_{assembler}"
    shell:"""
        multiqc {input} -o {params.directory} -n multiqc_report
    """


rule cd_hit_est:
    input:
        expand("{prefix}/fasta/{prefix}_{assembler}.fna", prefix=samples.index,assembler=config["assembler"])
    output:
        fasta = "result/pangenome_{assembler}.fna",
        complete = "result/pangenome_{assembler}_complete.fna"
    threads: 
        config["threads"]
    params:
        assembler = config["assembler"]
    envmodules:
       config["modules"]["cdhit"]
    shell:"""
        cat {input} > {output.complete};
        cd-hit-est -M 0 -i {output.complete} -o {output.fasta} -T {threads}
    """
