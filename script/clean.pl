#!/usr/bin/perl

my $file = shift;

open(IN,$file);

my $header = <IN>;

while(<IN>){
    chomp;
    my $id = (split(/\t/,$_))[0]; 
    my $blast_contig = $id ."/blast/".$id."_masurca_contig.out";
    my $blast_nt = $id ."/blast/".$id."_masurca_nt.out";
    my $blast_nt_back = $id ."/blast/".$id."_masurca_nt.out.bk";
    my $blast_reference = $id ."/blast/".$id."_masurca_reference.out";
    
    my $index_raw    = $id ."/fasta/".$id. "_masurca_raw.fasta.index";
    my $index_contig    = $id ."/fasta/".$id. "_masurca_filter_contig.fna.index";
    my $index_nt    = $id ."/fasta/".$id. "_masurca _filtered.fna.index";
    
    my $fasta_raw    = $id ."/fasta/".$id. "_masurca_raw.fasta";
    my $fasta_nt    = $id ."/fasta/".$id. "_masurca _filtered.fna";
    my $fasta_reference    = $id ."/fasta/".$id. "_masurca_filtered.fna"; 
    my $fasta_contig    = $id ."/fasta/".$id. "_masurca_filter_contig.fna";
    my $fasta_final =  $id ."/fasta/".$id. "_masurca.fna";
    my $fasta_final_megahit =  $id ."/fasta/".$id. "_megahit.fna";
    
    my $quast = $id ."/quast";
    my $cmd_quast_compare = join(" ","quast.py -o ",$quast," --threads 4", $fasta_final, $fasta_final_megahit);
    system("sparse -q agap_normal -c 'module load quast/5.0.2;$cmd_quast_compare'");
    
    
    
    my $keep_contig  = $id ."/blast/".$id."_masurca_to_keep_filter_contig.txt";
    my $keep_reference = $id ."/blast/".$id."_masurca_to_keep_reference.txt";
    my $keep_nt = $id ."/blast/".$id."_masurca_to_keep_nt.txt";
    
    my $remove_contig = $id ."/blast/".$id."_masurca_to_remove_filter_contig.txt";
    my $remove_reference = $id ."/blast/".$id."_masurca_to_remove_reference.txt";
    my $taxonomy = "/lustre/drocg/ncbi_nt_database/ncbi_taxonomy/expanded_ncbi_taxonomy.tsv";
    my $cmd_filter_contig = join(" ","perl script/filter_blast.pl",$blast_contig ,90  ,$index_raw ,$keep_contig, $remove_contig);
    #system("sparse -q agap_normal -c 'module load perllib;$cmd_filter_contig'");
   # system("sparse -q agap_normal -c 'module load meme;fasta-make-index $fasta_contig'");
    my $cmd_filter_reference = join(" ","perl script/filter_blast.pl",$blast_reference ,90  ,$index_contig ,$keep_reference, $remove_reference);
    #system("sparse -q agap_normal -c 'module load perllib;$cmd_filter_reference'");
    #my $command = join(" ","fasta-fetch", $fasta_contig, "-f", $keep_reference ," >", $fasta_reference);
    #system("sparse -q agap_normal -c 'module load meme;$command'");      
    my $directory = $id ."/taxonomy_masurca";
    my $directory_quast = $id ."/quast_masurca";
  #  system("sparse -q agap_normal -c 'module load perllib;$cmd_filter_contig'");
    my $tsv = $directory ."/taxonomy_assignment_per_sequence.tsv";
    my $lca = $directory ."/taxonomy_assignment_per_sequence.lca";
    my $html = $directory ."/taxonomy_assignment_per_sequence.html";
    my $cmd_quast = join(" ","quast.py -o ",$directory_quast," --threads 4", $fasta_raw, $fasta_contig,$fasta_reference,$fasta_final);
    #print $cmd_quast,"\n";
    my $cmd_basta = join(" ","singularity exec /nfs/work/agap_id-bin/img/basta/1.3.2.3/basta.1.3.2.3.img basta2krona.py", $lca ,$html);
   # system("sparse -q agap_normal -c 'module load quast/5.0.2;$cmd_quast'");
    #system("sparse -q agap_normal -c 'module load meme;$command'");
   # system("sparse -q agap_normal -c 'module load basta/1.3.2.3;$cmd_basta'");
    #print $command ,"\n"; 
    #system( "rm -Rf $directory");
    my $cmd_tax = join(" ","python3 script/taxonomy_assignment_BLAST.py --output_dir", $directory ,"--ncbi_nt  --blast_file", $blast_nt,$fasta_reference ,  $taxonomy);
   # print $cmd_tax,"\n";
    #system("sparse -q agap_normal -c 'module load snakemake/5.13.0;$cmd_tax'");
    my $cmd_tsv = "grep 'Streptophyta' $tsv  | cut -f 1 -d ' ' > $keep_nt";
    #system($cmd_tsv);
    my $cmd_lca = " cut -f 1,2 -d ' ' $tsv  | sed -e 's/ /\\t/g' > $lca";
    #system($cmd_lca);
    
    my $command_index = join(" ","fasta-make-index", $fasta_reference);
    #system("sparse -q agap_normal -c 'module load meme;$command_index'");
    my $command = join(" ","fasta-fetch", $fasta_reference, "-f", $keep_nt ," >", $fasta_final);
    #system("sparse -q agap_normal -c 'module load meme;$command'");
   # system( $cmd_tsv);#,"\n";
    #open(KEEP,$keep_reference);
    #my %contig;
    #while(<KEEP>){
    #    chomp;
    #    $contig{$_}=1;
    #}
    #close KEEP;
    #system("cp $blast_nt $blast_nt_back");
    #open(OUT,$blast_nt_back);
    #open(BLAST,">$blast_nt");
    #my $remove = 0;
    #while(<OUT>){
    #    chomp;
    #    my $contig_id = (split(/\t/,$_))[0];
    #    if ($contig{$contig_id}){
    #        print BLAST $_ ,"\n";
    #    }
    #    else { 
    #        $remove++;
    #    }
    #}
    #print join("\t",$id,$remove),"\n";
    #close BLAST;
    #last;
    #system('sparse -q agap_normal -c "perl script/filter_blast.pl $blast_contig 90 $index_raw $keep_contig $remove_contig"');
    
    
    
}
close IN;
