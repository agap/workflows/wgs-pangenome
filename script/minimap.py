#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)

import sys
import os
import subprocess
import argparse
import signal
import uuid
import shutil
garbage = False  # assume this is a good contig
min_pident=90
min_cov=90
minitmp="/home/drocg/scratch/funannotate_pangenome/minimap2"
with open(minitmp, 'r') as data:
    for line in data:
        line = line.replace('\n', '')
        qID, qLen, qStart, qEnd, strand, tID, tLen, tStart, tEnd, matches, alnLen, mapQ = line.split('\t')[:12]
        if qID != tID:
            pident = float(matches) / int(alnLen) * 100
            coverage = float(alnLen) / int(qLen) * 100
            # print qID, str(qLen), tID, matches, alnLen, str(pident), str(coverage)
            if pident > min_pident and coverage > min_cov:
                #print(("{} (length {}) appears duplicated with {} (length {}) : {:.0f}% identity over {:.0f}%".format(qID, qLen, tID,tLen,pident, coverage)))
                print(qID)
                #print(qID)
                garbage = True
                #break