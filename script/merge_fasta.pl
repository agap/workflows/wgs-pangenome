#!/usr/bin/perl

use Bio::SeqIO;
my $in = shift;
my $out = shift;

my $seqin = new Bio::SeqIO(
    -file => $in,
    -format => "fasta"
);
my $seqout = new Bio::SeqIO(
    -file => ">$out",
    -format => "fasta"
);

 

my $num_seq_by_batch = 30000; 
my $stretchN = "N" x 500; 
my $seq = "";
my $count = 0;
my %seq;
while (my $seqobj = $seqin->next_seq) {
     
    my $display_id =  (split(/\_/,$seqobj->display_id))[0];
    push @{$seq{$display_id}},$seqobj;
}
$seqin->close;
foreach my $id (keys%seq){
    my $seq = "";
   # print $id ,"\t",scalar(@{$seq{$id}}),"\n";
    foreach my $seqobj ( sort {$b->length <=> $a->length} @{$seq{$id}}) { 
        my $seq_length = $seqobj->length;
        $seq .= $seqobj->seq() . $stretchN; 
    }
    if ($seq ne ""){ 
        my $new_seqobj = new Bio::PrimarySeq(
            -id => $id,
            -seq =>$seq
        );
        $seqout->write_seq($new_seqobj);
    }
    else {
        print $id ,"\n";
    }
}
$seqout->close;
