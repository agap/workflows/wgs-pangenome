#!/usr/bin/perl
use Bio::SeqIO;
use File::Basename;

my $fasta = shift;
my ($name,$path,$suffix) = fileparse($fasta);
my $prefix = (split(/\_/, $name))[0];
print $prefix ,"\n";

my $output = $path."/".$prefix."_new_id.fasta";
print $output,"\n";

my $in = new Bio::SeqIO(
    -file => $fasta,
    -format => "fasta"
);
my $out = new Bio::SeqIO(
    -file => ">$output",
    -format => "fasta"
);

my $cpt_contig=0;
while(my $seqobj = $in->next_seq){
    $cpt_contig++;
    
    my $new_id = sprintf( "%s_%05d",$prefix, $cpt_contig * 10 );
    $seqobj->display_id($new_id);
    $seqobj->desc("");
    $out->write_seq($seqobj);
}
$in->close;
$out->close;

system("mv ". $output ." ". $fasta);