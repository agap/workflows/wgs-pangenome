#!/usr/bin/perl
use Bio::SeqIO;
my $lca = shift;
my $fasta = shift;
my $prefix = shift;
my $output = shift;

my $blast_nt = $prefix."/blast/".$prefix."_masurca_nt.out";
my $is_streptophyta = 0;
my $is_contaminant = 0;
my $cpt_contig = 0;
my $cpt_no_hit = 0;
my $length_streptophyta = 0;
my $length_cum = 0;
my %classification;
my %length;
my %organelle;
my $other = $prefix."/basta/".$prefix.".other.txt";
my $streptophyta = $prefix."/basta/".$prefix.".streptophyta.txt";
my $unknown = $prefix."/basta/".$prefix.".unknown.txt";
my $in = new Bio::SeqIO(
    -file => $fasta,
    -format => "fasta"
);
my $out = new Bio::SeqIO(
    -file => ">$output",
    -format => "fasta"
);
my $out_no_hit = new Bio::SeqIO(
    -file => ">$output.nohit",
    -format => "fasta"
);
my %contig;
while(my $seqobj = $in->next_seq){
    $cpt_contig++;
    $contig{$seqobj->display_id} = $seqobj;
}
$in->close;
open(IN,$blast_nt);
my %hsp;
while(<IN>){
    chomp;
    my $id = (split(/\t/,$_))[0];
    push @{$hsp{$id}} , $_;
}
close IN;
my %streptophyta;
open(IN,$lca);
open(OTHER,">$other");
open(STREPTO,">$streptophyta");
open(UNK,">$unknown");

while(<IN>){
    chomp;
    my ($id,$taxonomy) = (split(/\t/,$_));
    my @hierarchy = (split(/\;/,$taxonomy));
    if ($hierarchy[1] eq "Streptophyta") {
        $is_streptophyta++;
        my $new_id = sprintf( "%s_%05d",$prefix, $is_streptophyta * 10 );
        $contig{$id}->display_id($new_id);
        $out->write_seq($contig{$id});
        
        print STREPTO  join("\n",@{$hsp{$id}}),"\n";
        delete($hsp{$id});
        delete($contig{$id});
    }
    else {
        $is_contaminant++;
        print OTHER  join("\n",@{$hsp{$id}}),"\n";
        delete($hsp{$id});
        delete($contig{$id});
        
    }
}
$out->close;
close IN;
foreach my $id(keys %hsp){
    print UNK join("\n",@{$hsp{$id}}),"\n";
    delete($contig{$id});
    $unknown++;
}

foreach my $id (keys %contig){
    $cpt_no_hit++;
    #print join("\t",$id,$contig{$id}->length),"\n";
    $out_no_hit->write_seq($contig{$id});
}
$out_no_hit->close;
    