#!/usr/bin/perl

use strict;
use Data::Dumper;
 
my $file = shift; 

print "export and sort bed file \n"; 
open F, $file or die "pas possible 0\n";
open Sav, ">$file.bed" or die "pas possible 0.1\n";
while(<F>) {
 
	my @data = split /\t/, $_; 
	if ($data[2] >=90)
	{
		print Sav "$data[0]\t$data[6]\t$data[7]\n";
	 }	
}
close F;
close Sav;

### Tri des donnees par position
system("sort -k1,1 -k2,2n $file.bed > $file.sorted.0.9.bed");


print "bedtool merge step\n";
#system("module load bioinfo/bedtools/2.29.0; bedtools merge -d 299 -i $rep/contig2ref.bed | sort -k1,1 -k2,2n > $rep/contig2ref_merge.bed");
system("bedtools merge -d 299 -i $file.sorted.0.9.bed | sort -k1,1 -k2,2n > $file.merge.0.9.bed");

print "Load contig list and size\n";
system(" samtools faidx final.contigs.fa");

my %id_contig;
open F, "final.contigs.fa.fai" or die "pas possible 1\n";
while(<F>) {
    chomp;
    my $data=$_; 
	my @data = split /\s/, $data; 
	$id_contig{$data[0]}=$data[1];

}
close F;

print "Parse merged bed\n";

my $name="";
my $posdeb=1;
my $posfin=1;
my $length=1;
my $cpt=0;
my $covreal;
my %match;
open IN2, "$file.merge.0.9.bed" or die "pas possible 2\n"; 
while(<IN2>) {
    chomp;
    my $data=$_;
	my @data = split /\t/, $data; 
	if ($data[0] eq $name) {
		if ($data[1] - $posfin >=499){
			$match{$name}{'substr'}{$posfin +1} = $data[1] -1;
			my $tmp=$data[1]-$posfin;
		}
		if ($data[2] - $data[1] >=299) {
			$match{$name}{'match'} = 1;
		}
		if ($data[1] >= $posfin) {
			$covreal+=$data[2] - $data[1];
			$posfin=$data[2];
		}
		elsif ($data[2] > $posfin) {
			$covreal+=$data[2]-$posfin;
			$posfin=$data[2];
		}
	}
	else {	
		if ($data[1] - 1 >=499){
			$match{$data[0]}{'substr'}{1} = $data[1] -1;
			my $tmp=$data[1] - 1; 
		}
		if ($data[2] - $data[1] >=299){
			$match{$data[0]}{'match'} = 1;
		}
		if ($length - $posfin >=499) {
			$match{$name}{'substr'}{$posfin+1} = $length;
			my $tmp=$length-$posfin; 
		}
		if ($covreal >=299) {
			$match{$name}{'match'} = 1;
		}
		if ($covreal > $length){
			print "probleme sur $name : cov=$covreal - length=$length\n"; exit;
		} 		
		$covreal = $data[2] - $data[1] + 1 ; 

		$name=	$data[0];
		$posfin=$data[2];
		$length=$id_contig{$data[0]};
		$cpt++;	
	}
}

close IN2;

print "Export parse results \n";

open Sav2, ">$file.id_contig_without_blast.0.9.txt" or die "pas possible1\n";
open Sav3, ">$file.id_contig_partial_blast.0.9.txt" or die "pas possible2\n";
open Sav4, ">$file.id_contig_reliable_blast.0.9.txt" or die "pas possible3\n";

while (my ($id_contig,$valeur)=each %id_contig) {
	if (exists $match{$id_contig}{'match'}) {
		if (exists $match{$id_contig}{'substr'}) {
			my $hash = $match{$id_contig}{'substr'};
			my %hash=%$hash;
			while (my ($deb,$fin)=each %hash) {
				print Sav3 "$id_contig\t$deb\t$fin\n";
			}
		}
		else {print Sav4 "$id_contig\n";}
	}
	else {print Sav2 "$id_contig\n";}
}	

close Sav2;
close Sav3;
close Sav4;

print "bedtools getfasta contig_partial_blast\n";
system (" bedtools getfasta -fi final.contigs.fa -bed $file.id_contig_partial_blast.0.9.txt > $file.contig_partial_blast.0.9.fna");


print "index contigs file\n";
system("/home/drocg/bin/fasta-make-index final.contigs.fa");

print "fasta-fetch contig_without_blast\n";
system("/home/drocg/bin/fasta-fetch final.contigs.fa -f $file.id_contig_without_blast.0.9.txt > $file.contig_without_blast.0.9.fna");

