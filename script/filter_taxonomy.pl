#!/usr/bin/perl

my $file_in = shift;  
my $file_out = shift;


open(IN,$file_in);
open(OUT,">$file_out"); 
while(<IN>){
    chomp; 
    my ($qseqid,$taxonomy) = (split(/\s/,$_))[0,1];
    my $is_streptophyta = (split(/\;/,$taxonomy))[2];
    if ($is_streptophyta eq "Streptophyta") { 
        print OUT $qseqid ,"\n";
    }
}
close IN;
close OUT;