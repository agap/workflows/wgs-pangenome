#!/usr/bin/perl

my $file_in = shift;
my $cuttoff = shift ;
my $index = shift;
my $file_out = shift;
my $file_log = shift;


open(IN,$file_in);
open(OUT,">$file_out");
open(LOG,">$file_log");
my @hsp;
while(<IN>){
    chomp; 
    my ($qseqid,$sseqid ,$qlen,$slen,$length,$qstart,$qend,$sstart,$send,$pident,$evalue) = (split(/\t/,$_));
    next if $qseqid eq $sseqid;
    if ($qlen < $slen) {
        push @{$hsp{$qseqid}{$sseqid}} , {
            qstart => $qstart,
            qend   => $qend,
            qlen => $qlen,
            slen => $slen,
            sstart => $sstart,
            send   => $send
        };
    }
}
close IN;
open(IN,$index);
my %seq_id; 
while(<IN>){
    chomp;
    my $id = (split(/\s/,$_))[0]; 
    $id =~ s/^>//;
    $seq_id{$id} = 1;
}
close IN; 
my %remove;
foreach my $contig (keys %hsp){
    foreach my $hit (keys %{$hsp{$contig}}){ 
        my $latest;
        my $size;
        my $qlen = $hsp{$contig}{$hit}[0]->{qlen};
        my $slen = $hsp{$contig}{$hit}[0]->{slen};
        foreach my $hsp (sort {$a->{qstart} <=> $b->{qstart}} @{$hsp{$contig}{$hit}}) {
            if ($latest) {
                if ($hsp ){
                    if ($hsp->{qstart} > $latest->{qend}) { 
                        $size += abs($hsp->{qstart} - $hsp->{qend}) + 1;
                        $latest = $hsp;
                    }
                }
                else {
                    next;
                }
            }
            else {
                $latest = $hsp; 
                $size = abs($hsp->{qstart} - $hsp->{qend}) + 1;
            }
        } 
        my $percent    = sprintf('%.2f',($size / $qlen)) * 100;
        if ($percent > $cuttoff) { 
            $remove{$contig} = 1;
            delete($seq_id{$contig});
        }   
    }
}

foreach my $id (keys%seq_id){
    print OUT $id,"\n";
}
close OUT;
foreach my $id (keys%remove){
    print LOG $id,"\n";
}
close LOG;