#!/usr/bin/perl
use File::Basename;
use Getopt::Long;
use FindBin;
use Pod::Usage;

my @directory;
my $directory;
my $separator = "_";
my $help;

my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters

    -directory   Directory where fastq.gz file are stored, can be multiple (mandatory)
    -separator    Default "_"
    -help
/;
GetOptions(
    'directory=s@'  => \$directory,
    'separator=s'    => \$separator,
    'help|h|?'     => \$help
)  or pod2usage(-message => $usage);

if ($directory eq "") {
    warn "\nWarn :: --directory is empty. Directory where fastq.gz file are stored, can be multiple (mandatory)\n\n";
    warn $usage;
    exit 0;
}
my @dir = @$directory;
my $output = "metadata.tsv";
open(OUT,">$output");
print OUT join("\t","sample","prefix","fq1","fq2"),"\n";
my %data;
foreach my $dir (@dir) {
    open(IN,"ls $dir/*.gz|");
    print $dir,"\n";
    my %fastq;
    while(my $file = <IN>){
        chomp($file);
    
        my ($name,$path) = fileparse($file);
        my @value = (split(/\_/,$name));
        pop(@value);
        my $prefix =  join("_",@value);
        my $forward;
        my $reverse;
        if ($name =~ /.*1.fastq.gz/) {
            $data{$prefix}{forward} = $file;
        }
        if ($name =~ /.*2.fastq.gz/) {
            $data{$prefix}{reverse} = $file;
        }
     
    
    }
    close IN;
}
foreach my $prefix (keys %data){
    print OUT join("\t",$prefix,$prefix,$data{$prefix}{forward},$data{$prefix}{reverse}),"\n";
}
close OUT;
 