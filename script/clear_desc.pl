#!/usr/bin/perl

use Bio::SeqIO;
 
my $in = shift;
my $out = shift;

my $seqin = new Bio::SeqIO(
    -file => $in,
    -format => "fasta"
);
my $seqout = new Bio::SeqIO(
    -file => ">$out",
    -format => "fasta"
);

while (my $seqobj = $seqin->next_seq) {
    $seqobj->desc("");
    $seqout->write_seq($seqobj);
}
$seqin->close;
$seqout->close;